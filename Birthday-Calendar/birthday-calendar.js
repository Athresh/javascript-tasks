function CalendarApp() {
    const mappedDayOfWeek = ["sun", "mon", "tue", "wed", "thu", "fri", "sat"]
    const WEEK_DATA = { sun: [], mon: [], tue: [], wed: [], thu: [], fri: [], sat: [] }
    const parentSquareSide = 160
    const dateFormats = ['DD/MM/YYYY', 'MM/DD/YYYY', 'YYYY/DD/MM', 'YYYY/MM/DD']
    
    this.prevRawData = null
    this.prevRequestedYear = null
    this.init = function () {
        this.registerListeners()
        this.retrieveValues()
    }

    this.registerListeners = function () {
        const calForm = document.getElementById('cal_form')
        calForm.addEventListener('submit', evt => {
            evt.preventDefault()
            this.retrieveValues()
        })
    }
    
    this.retrieveValues = function () {
        this.throwError()
        if(!window.moment) {
            return
        }
        
        const yearEl = document.getElementById("year_field")
        let requestedYear = yearEl.value
        const rawData = document.getElementById("data_field").value

        let data = []
        try {
            if (rawData) {
                data = JSON.parse(rawData)
                if(!Array.isArray(data)) {
                    throw new Error('Array expected')
                }
            } else {
                data = []
            }
        } catch (er) {
            this.throwError("Input data is invalid!")
            data = []
        }

        if (this.prevRawData !== rawData || this.prevRequestedYear !== requestedYear) {
            this.prevRawData = rawData
            this.prevRequestedYear = String(requestedYear)
            this.segregateData(data, requestedYear)
        }
    }

    this.segregateData = function (data, requestedYear) {
        let weekData = {...WEEK_DATA}
        data.forEach((person) => {
            if (person && person.name && person.birthday && requestedYear) {
                try {
                    const isDateValid = moment(person.birthday, dateFormats).isValid()
                    if(!isDateValid) return
                    const bdayOnRequestedYear = moment(person.birthday, dateFormats).year(requestedYear)
                    const dayOfWeek = bdayOnRequestedYear.day()
                    if (!isNaN(dayOfWeek) && typeof dayOfWeek === "number") {
                       const day = mappedDayOfWeek[dayOfWeek]
                       const sortedDataOfDay = this.sortByDate([...weekData[day], { ...person }])
                       weekData = {
                            ...weekData,
                            [day]: [...sortedDataOfDay],
                        }
                    }
                } catch (er) {
                    console.log(er)
                    return
                }
            }
        })
        this.renderApp(weekData)
    }

    this.renderApp = function (weekData) {
        let weekEl = ""
        for (const day in weekData) {
            if (weekData.hasOwnProperty(day)) {
                const dayData = weekData[day]
                const isEmpty = Array.isArray(dayData) ? dayData.length === 0 : true
                weekEl += `<div class="cal__day flex ${isEmpty ? "day--empty" : ""}">
                                <div class="cal__day_header">${day}</div>
                                <div class="cal__day_content">
                                    ${this.renderBdayCard(dayData)}
                                </div>
                            </div>`
            }
        }
        document.getElementById("weekly").innerHTML = weekEl
    }
    
    this.renderBdayCard = function (dayData) {
        if (!Array.isArray(dayData)) return ''
        let dayCards = ""
        const gridSize = Math.ceil(Math.sqrt(dayData.length))
        const cardWidth = parseFloat(parentSquareSide / gridSize).toFixed('5');
        const fontSize = cardWidth <= 25 ? `${parseInt(cardWidth/1.8)}px` : 'inherit'
        dayData.forEach((person) => {
                const cardStyle = `background-color: ${this.generateColor()}; width: ${cardWidth}px; height:${cardWidth}px; font-size:${fontSize}`

                dayCards += `<div class="day__person" title='${person.name}, on ${person.birthday}' style="${cardStyle}">
                    ${this.getInitials(person.name)}
                </div>`
        })
        return dayCards
    }
    
    this.getInitials = function (fullName = "") {
        if (!fullName) return ""
        fullName = String(fullName)
        if (fullName.indexOf(" ") > -1) {
            return `${fullName[0]}${fullName[fullName.indexOf(" ") + 1]}`
        }
        return fullName[0]
    }
    
    this.generateColor = function () {
        var lum = -0.25
        var hex = String("#" + Math.random().toString(16).slice(2, 8).toUpperCase()).replace(/[^0-9a-f]/gi, "")
        if (hex.length < 6) {
            hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2]
        }
        var rgb = "#",
            c,
            i
        for (i = 0; i < 3; i++) {
            c = parseInt(hex.substr(i * 2, 2), 16)
            c = Math.round(Math.min(Math.max(0, c + c * lum), 255)).toString(16)
            rgb += ("00" + c).substr(c.length)
        }
        return rgb
    }

    this.sortByDate = function (dayData) {
        return dayData.sort((a, b) => new Date(b.birthday) - new Date(a.birthday))
    }

    this.throwError = function (msg) {
        const errorEl = document.getElementById("error-msg")
        if(msg) {
            errorEl.innerText = `Error: ${msg}`
        } else {
            errorEl.innerText = ``
        }
    }
}

let calendarInstance = new CalendarApp()

calendarInstance.init()