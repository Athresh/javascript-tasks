const myArray = ['2','3','1','7','69','24','56','68','99']

function myFilter(myArray, callback) {
  return callback(myArray)
}

const oddArray = function(myArray){
  return myArray.filter(n => n % 2)
}
  
const evenArray = function(myArray){
  return myArray.filter(n => n % 2 == 0)
}   
  
console.log("oddArray",myFilter(myArray,oddArray))
console.log("evenArray",myFilter(myArray,evenArray))
  